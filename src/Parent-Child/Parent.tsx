import Child from "./Child";

const Parent = () => {


  const info = {
    imgSrc: "https://picsum.photos/200",
    title: "Information",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  };

  return (
    <>
      <Child 
      imgSrc={info.imgSrc} 
      title={info.title} 
      text={info.text} />
    </>
  );
};

export default Parent;
