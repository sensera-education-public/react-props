import "./form.css";

import { Button, TextField, Select, MenuItem } from "@mui/material";
import { useEffect, useState } from "react";

interface IForm {
  fname: string;
  lname: string;
  email: string;
  pass: string;
  role: string;
}

export default function Form() {
  const [formData, setFormData] = useState<IForm>({
    fname: "",
    lname: "",
    email: "",
    pass: "",
    role: "",
  });
  const [formErrors, setFormErrors] = useState<Partial<IForm>>({
    fname: "",
    lname: "",
    email: "",
    pass: "",
    role: "",
  });

  const validate = () => {
    const errors: Partial<IForm> = {};
    if (!formData.fname) {
      errors.fname = "Förnamn behöver fyllas i";
    }  if (!formData.lname) {
      errors.lname = "Efternamn behöver fyllas i";
    }
    if (!formData.email) {
      errors.email = "E-mail behöver fyllas i";
    } else if (!/\S+@\S+\.\S+/.test(formData.email)) {
      errors.email = "Det här är inte en giltig e-post adress";
    }
    if (!formData.pass) {
      errors.pass = "Du behöver fylla i lösenord";
    }
    if (!formData.role) {
      errors.role = "Du behöver välja roll";
    }
    setFormErrors(errors);
    return Object.keys(errors).length === 0;
  };

  function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    validate();
    console.log(formData);
  }

  function handleChange(name: string, value: string) {
    setFormData({
      ...formData,
      [name]: value,
    });
  }

  return (
    <>
      <form onSubmit={handleSubmit}>
        <h2 className="label-wrapper">Registrera dig</h2>
        <div className="form">
          <p>
            Förnamn <span>*</span>
          </p>
          <TextField
            id="outlined-basic"
            variant="outlined"
            className="input input__lg"
            name="fname"
            autoComplete="off"
            value={formData.fname}
            onChange={(e) => handleChange(e.target.name, e.target.value)}
          />
          {formErrors.fname && <div className="error">{formErrors.fname}</div>}
          <p>
            Efternamn <span>*</span>
          </p>
          <TextField
            id="outlined-basic"
            variant="outlined"
            className="input input__lg"
            name="lname"
            autoComplete="off"
            value={formData.lname}
            onChange={(e) => handleChange(e.target.name, e.target.value)}
          />
          {formErrors.lname && <div className="error">{formErrors.lname}</div>}
          <p>
            E-mail <span>*</span>
          </p>
          <TextField
            id="outlined-basic"
            variant="outlined"
            className="input input__lg"
            name="email"
            autoComplete="off"
            value={formData.email}
            onChange={(e) => handleChange(e.target.name, e.target.value)}
          />
          {formErrors.email && <div className="error">{formErrors.email}</div>}
          <p>
            Lösenord <span>*</span>
          </p>
          <TextField
            id="outlined-basic"
            variant="outlined"
            className="input input__lg"
            name="pass"
            autoComplete="off"
            value={formData.pass}
            onChange={(e) => handleChange(e.target.name, e.target.value)}
          />
          {formErrors.pass && <div className="error">{formErrors.pass}</div>}
          <p>
            Roll <span>*</span>
          </p>
          <Select
            labelId="demo-simple-select-helper-label"
            id="demo-simple-select-helper"
            value={formData.role}
            label="role"
            name="role"
            onChange={(e) => handleChange(e.target.name, e.target.value)}
          >
            <MenuItem value={"cleaner"}>Städare</MenuItem>
            <MenuItem value={"client"}>Kund</MenuItem>
          </Select>
          {formErrors.role && <div className="error">{formErrors.role}</div>}
          <Button variant="contained" type="submit" className="btn">
            Skapa Konto
          </Button>
        </div>
      </form>
    </>
  );
}
